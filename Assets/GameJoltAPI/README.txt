# Unity Game Jolt API
# Version: 1.2.4

# Author: Unsymmetrical Something
# Author URL: http://unsymsth.com

# Home Page: http://gamejolt.com/games/other/unity-game-jolt-api/15887/
# Forum Post: http://gamejolt.com/community/forums/topics/unity-api/1803/
# Bug Tracking: https://bitbucket.org/loicteixeira/unity-game-jolt-api/issues/
# API Tutorial: http://wp.me/p2PWEz-9g/
# Helper Tutorial: http://wp.me/p2PWEz-8P/
# Demo/Documentation: http://gamejolt.com/games/other/unity-gamejolt-api-demo/14443/