﻿using UnityEngine;
using System.Collections;

public class GameJoltAPIManager : MonoBehaviour
{
	public int gameID;
	public string privateKey;
	public string userName;
	public string userToken;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		GJAPI.Init ( gameID, privateKey );
		GJAPIHelper.Users.GetFromWeb(OnGetFromWeb);
		GJAPI.Users.VerifyCallback += OnVerifyUser;
	}

	void OnGetFromWeb (string name, string token)
	{
		userName=name;
		userToken=token;
		GJAPI.Users.Verify(userName,userToken);
	}

	void OnEnable()
	{
		GJAPI.Users.VerifyCallback += OnVerifyUser;
	}
	
	void OnDisable()
	{
		GJAPI.Users.VerifyCallback -= OnVerifyUser;
	}

	void OnVerifyUser(bool success)
	{
		if(success)
		{
			GJAPIHelper.Users.ShowGreetingNotification ();
		}
		else
		{
			Debug.Log("Um... Something went wrong.");
		}
	}
}
