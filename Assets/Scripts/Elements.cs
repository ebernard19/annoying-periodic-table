﻿using UnityEngine;
using System.Collections.Generic;

public class Elements : MonoBehaviour
{
	public static string[] elemList=new string[118];
	public static int[] created=new int[118];

	// Use this for initialization
	void Start ()
	{
		int i;

		elemList[0]="h";
		elemList[1]="he";
		elemList[2]="li";
		elemList[3]="be";
		elemList[4]="b";
		elemList[5]="c";
		elemList[6]="n";
		elemList[7]="o";
		elemList[8]="f";
		elemList[9]="ne";
		elemList[10]="na";
		elemList[11]="mg";
		elemList[12]="al";
		elemList[13]="si";
		elemList[14]="p";
		elemList[15]="s";
		elemList[16]="cl";
		elemList[17]="ar";
		elemList[18]="k";
		elemList[19]="ca";
		elemList[20]="sc";
		elemList[21]="ti";
		elemList[22]="v";
		elemList[23]="cr";
		elemList[24]="mn";
		elemList[25]="fe";
		elemList[26]="co";
		elemList[27]="ni";
		elemList[28]="cu";
		elemList[29]="zn";
		elemList[30]="ga";
		elemList[31]="ge";
		elemList[32]="as";
		elemList[33]="se";
		elemList[34]="br";
		elemList[35]="kr";
		elemList[36]="rb";
		elemList[37]="sr";
		elemList[38]="y";
		elemList[39]="zr";
		elemList[40]="nb";
		elemList[41]="mo";
		elemList[42]="tc";
		elemList[43]="ru";
		elemList[44]="rh";
		elemList[45]="pd";
		elemList[46]="ag";
		elemList[47]="cd";
		elemList[48]="in";
		elemList[49]="sn";
		elemList[50]="sb";
		elemList[51]="te";
		elemList[52]="i";
		elemList[53]="xe";
		elemList[54]="cs";
		elemList[55]="ba";
		elemList[56]="la";
		elemList[57]="ce";
		elemList[58]="pr";
		elemList[59]="nd";
		elemList[60]="pm";
		elemList[61]="sm";
		elemList[62]="eu";
		elemList[63]="gd";
		elemList[64]="tb";
		elemList[65]="dy";
		elemList[66]="ho";
		elemList[67]="er";
		elemList[68]="tm";
		elemList[69]="yb";
		elemList[70]="lu";
		elemList[71]="hf";
		elemList[72]="ta";
		elemList[73]="w";
		elemList[74]="re";
		elemList[75]="os";
		elemList[76]="ir";
		elemList[77]="pt";
		elemList[78]="au";
		elemList[79]="hg";
		elemList[80]="tl";
		elemList[81]="pb";
		elemList[82]="bi";
		elemList[83]="po";
		elemList[84]="at";
		elemList[85]="rn";
		elemList[86]="fr";
		elemList[87]="ra";
		elemList[88]="ac";
		elemList[89]="th";
		elemList[90]="pa";
		elemList[91]="u";
		elemList[92]="np";
		elemList[93]="pu";
		elemList[94]="am";
		elemList[95]="cm";
		elemList[96]="bk";
		elemList[97]="cf";
		elemList[98]="es";
		elemList[99]="fm";
		elemList[100]="md";
		elemList[101]="no";
		elemList[102]="lr";
		elemList[103]="rf";
		elemList[104]="db";
		elemList[105]="sg";
		elemList[106]="bh";
		elemList[107]="hs";
		elemList[108]="mt";
		elemList[109]="ds";
		elemList[110]="rg";
		elemList[111]="cn";
		elemList[112]="uut";
		elemList[113]="fl";
		elemList[114]="uup";
		elemList[115]="lv";
		elemList[116]="uus";
		elemList[117]="uuo";

		for(i=0;(i<118);i++)
		{
			created[i]=0;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
