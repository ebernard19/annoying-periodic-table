﻿using UnityEngine;
using System.Collections;

public class CreatedElements : MonoBehaviour
{
	private Transform table;

	// Use this for initialization
	void Start()
	{
		table=GameObject.Find("Table").transform;
	}
	
	// Update is called once per frame
	void Update()
	{
		int i;

		foreach(Transform child in table)
		{
			for(i=0;(i<118);i++)
			{
				if(child.name==Elements.elemList[i])
				{
					if(Elements.created[i]==0)
					{
						child.gameObject.renderer.enabled=false;
					}
					else
					{
						child.gameObject.renderer.enabled=true;
					}
				}
			}
		}
	}
}
