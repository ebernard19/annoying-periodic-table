﻿using UnityEngine;
using System.Collections;

public class MusicButtonClick : MonoBehaviour
{
	public GameObject sprite_1;
	public GameObject sprite_2;

	// Use this for initialization
	void Start ()
	{
		sprite_1.SetActive(true);
		sprite_2.SetActive(false);
	}

	void OnMouseDown()
	{
		sprite_1.SetActive(!sprite_1.activeSelf);
		sprite_2.SetActive(!sprite_2.activeSelf);

		if(sprite_1.activeSelf==true)
		{
			Camera.main.SendMessage("StartMusic");
		}
		else
		{
			Camera.main.SendMessage("StopMusic");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
